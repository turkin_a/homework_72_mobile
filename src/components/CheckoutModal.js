import React, {Component} from 'react';
import {connect} from "react-redux";
import {Modal, Text, TouchableOpacity, View, StyleSheet, ScrollView} from 'react-native';

import {createOrder, removeItemFromOrder} from "../store/actions";

class CheckoutModal extends Component {
  state = {
    modalVisible: false
  };

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  render() {
    return (
      <View>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            null;
          }}>
          <View style={styles.modal}>
            <View>
              <Text style={[styles.modalTitle, styles.bold]}>Your order</Text>
            </View>

            <ScrollView style={styles.modalList}>
              {this.props.order ? Object.keys(this.props.order).map(id => (
                <View style={styles.modalItem} key={id}>
                  <Text style={styles.modalItemName}>{this.props.dishList[id].name}</Text>
                  <Text style={[styles.modalItemValue, styles.bold]}>x {this.props.order[id]}</Text>
                  <Text style={styles.modalItemCost}>
                    {this.props.dishList[id].cost * this.props.order[id]} KGS
                  </Text>
                  <TouchableOpacity style={styles.modalItemDelete}
                    onPress={() => this.props.deleteItem(id)}>
                    <Text style={[styles.modalItemDeleteText, styles.bold]}>X</Text>
                  </TouchableOpacity>
                </View>
              )) : null}
              <View style={styles.modalSummary}>
                <Text style={styles.bold}>Delivery:</Text>
                <Text style={styles.bold}>150 KGS</Text>
              </View>
              <View style={styles.modalSummary}>
                <Text style={styles.bold}>Total:</Text>
                <Text style={styles.bold}>{this.props.totalPrice + 150} KGS</Text>
              </View>
            </ScrollView>

            <TouchableOpacity style={styles.button}
              onPress={() => {
                this.setModalVisible(!this.state.modalVisible);
              }}>
              <View>
                <Text>Cancel</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={styles.button}
              onPress={() => {
                this.setModalVisible(!this.state.modalVisible);
                this.props.confirmOrder();
              }}>
              <View>
                <Text>Order</Text>
              </View>
            </TouchableOpacity>
          </View>
        </Modal>

        <TouchableOpacity
          style={styles.checkoutWrap}
          onPress={() => {
            this.setModalVisible(true);
          }}>
            <Text style={styles.checkoutBtn}>Checkout</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    margin: 10,
    alignItems: 'center'
  },
  modalTitle: {
    fontSize: 20,
    marginVertical: 10
  },
  modalList: {
    width: '100%',
    borderRadius: 10,
    marginBottom: 20,
    paddingHorizontal: 20,
    backgroundColor: '#eee'
  },
  modalItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 10
  },
  modalItemName: {
    flex: 3
  },
  modalItemValue: {
    flex: 1,
    textAlign: 'right'
  },
  modalItemCost: {
    flex: 2,
    marginRight: 10,
    textAlign: 'right'
  },
  modalItemDelete: {
    flex: 1
  },
  modalItemDeleteText: {
    fontSize: 20,
    color: 'red',
    textAlign: 'right'
  },
  modalSummary: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10
  },
  button: {
    width: '50%',
    alignItems: 'center',
    marginBottom: 10,
    paddingVertical: 10,
    paddingHorizontal: 20,
    backgroundColor: 'lightblue',
    borderRadius: 4
  },
  checkoutWrap: {
    margin: 0
  },
  checkoutBtn: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    backgroundColor: 'lightblue',
    borderRadius: 4
  },
  bold: {
    fontWeight: 'bold'
  }
});

const mapStateToProps = state => {
  return {
    dishList: state.dishList,
    order: state.order,
    totalPrice: state.totalPrice
  };
};

const mapDispatchToProps = dispatch => {
  return {
    deleteItem: (id) => dispatch(removeItemFromOrder(id)),
    confirmOrder: () => dispatch(createOrder())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutModal);