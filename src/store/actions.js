import axios from '../../axios-pizzeria';

export const LOAD_DISHES = 'LOAD_DISHES';
export const ADD_DISH_TO_ORDER = 'ADD_DISH_TO_ORDER';
export const REMOVE_ITEM_FROM_ORDER = 'REMOVE_ITEM_FROM_ORDER';
export const CLEAR_ORDER = 'CLEAR_ORDER';

export const loadDishes = dishList => {
  return {type: LOAD_DISHES, dishList};
};

export const addDishToOrder = id => {
  return {type: ADD_DISH_TO_ORDER, id};
};

export const removeItemFromOrder = id => {
  return {type: REMOVE_ITEM_FROM_ORDER, id};
};

export const clearOrder = () => {
  return {type: CLEAR_ORDER};
};

export const getDishes = () => {
  return dispatch => {
    axios.get('dishes.json').then(response => {
      dispatch(loadDishes(response.data));
    }, error => {
      alert(error);
    });
  }
};

export const createOrder = () => {
  return (dispatch, getState) => {
    axios.post('orders.json', getState().order).then(() => {
      dispatch(clearOrder());
    }, error => {
      alert(error);
    });
  };
};
