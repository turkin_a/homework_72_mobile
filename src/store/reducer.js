import {ADD_DISH_TO_ORDER, CLEAR_ORDER, LOAD_DISHES, REMOVE_ITEM_FROM_ORDER} from "./actions";

const initialState = {
  dishList: {},
  order: {},
  totalPrice: 0
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_DISHES:
      return {...state, dishList: action.dishList};
    case ADD_DISH_TO_ORDER:
      const newOrder = {...state.order};
      newOrder[action.id] = state.order[action.id] ? state.order[action.id] + 1 : 1;
      const price = Object.keys(newOrder).reduce(((sum, id) => sum + newOrder[id] * state.dishList[id].cost), 0);
      return {...state, order: newOrder, totalPrice: price};
    case REMOVE_ITEM_FROM_ORDER:
      const order = {...state.order};
      delete order[action.id];
      const totalPrice = Object.keys(order).reduce(((sum, id) => sum + order[id] * state.dishList[id].cost), 0);
      return {...state, order, totalPrice};
    case CLEAR_ORDER:
      return {...state, order: {}, totalPrice: 0};
    default:
      return state;
  }
};

export default reducer;