import React, {Component} from 'react';
import {connect} from "react-redux";
import {ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';

import Item from "./src/components/Item";
import CheckoutModal from "./src/components/CheckoutModal";

import {addDishToOrder, getDishes} from "./src/store/actions";

class Application extends Component {
  componentDidMount() {
    this.props.loadContent();
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.title}>Turtle Pizza</Text>
        </View>
        <ScrollView style={styles.dishesItems}>
          {this.props.dishList ? Object.keys(this.props.dishList).map(id => (
            <TouchableOpacity style={styles.dishesList}
              key={id}
              id={id}
              onPress={() => {
                this.props.addDish(id);
              }}>
              <Item
                dish={this.props.dishList[id]}
              />
            </TouchableOpacity>
          )) : null}
        </ScrollView>
        <View style={styles.checkout}>
          <Text>Order total: {this.props.totalPrice} KGS</Text>
          <CheckoutModal />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    paddingHorizontal: 10
  },
  header: {
    width: '90%',
    borderBottomWidth: 1,
    borderBottomColor: '#333',
    marginBottom: 10
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold'
  },
  dishesItems: {
    width: '90%'
  },
  dishesList: {
    alignItems: 'center'
  },
  checkout: {
    width: '90%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderTopWidth: 1,
    borderTopColor: '#333',
    marginTop: 10,
    paddingVertical: 20
  }
});

const mapStateToProps = state => {
  return {
    dishList: state.dishList,
    order: state.order,
    totalPrice: state.totalPrice
  };
};

const mapDispatchToProps = dispatch => {
  return {
    loadContent: () => dispatch(getDishes()),
    addDish: (id) => dispatch(addDishToOrder(id))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Application);