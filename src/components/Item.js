import React from 'react';
import {Text, View, StyleSheet, Image} from "react-native";

const Item = props => (
  <View style={styles.item}>
    <View>
      <Image resizeMode="contain" source={{uri: props.dish.url}} style={styles.image} />
    </View>
    <View>
      <Text style={styles.name}>{props.dish.name}</Text>
      <Text>{props.dish.cost} KGS</Text>
    </View>
  </View>
);

const styles = StyleSheet.create({
  item: {
    width: '90%',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#eee',
    marginBottom: 10,
    padding: 10,
    paddingRight: 70,
    borderRadius: 10
  },
  image: {
    width: 50,
    height: 50,
    marginRight: 10,
    borderRadius: 10
  },
  name: {
    fontWeight: 'bold'
  }
});

export default Item;